---
title: "Basquiat"
date: 2022-10-08T14:53:10-03:00
draft: false
---

Jean-Michel Basquiat (December 22, 1960 – August 12, 1988) was an American artist who rose to success during the 1980s as part of the Neo-expressionism movement. Basquiat's art focused on dichotomies such as wealth versus poverty, integration versus segregation, and inner versus outer experience. He appropriated poetry, drawing, and painting, and married text and image, abstraction, figuration, and historical information mixed with contemporary critique. He used social commentary in his paintings as a tool for introspection and for identifying with his experiences in the Black community, as well as attacks on power structures and systems of racism. His visual poetics were acutely political and direct in their criticism of colonialism and support for class struggle.