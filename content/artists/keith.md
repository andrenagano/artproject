---
title: "Keith"
date: 2022-10-08T14:53:24-03:00
draft: false
---

Keith Allen Haring (May 4, 1958 – February 16, 1990) was an American artist whose pop art emerged from the New York City graffiti subculture of the 1980s. His animated imagery has "become a widely recognized visual language". Much of his work includes sexual allusions that turned into social activism by using the images to advocate for safe sex and AIDS awareness. In addition to solo gallery exhibitions, he participated in renowned national and international group shows such as documenta in Kassel, the Whitney Biennial in New York, the São Paulo Biennial, and the Venice Biennale. The Whitney Museum held a retrospective of his art in 1997